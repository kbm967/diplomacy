#!/usr/bin/env python3

# ------------------------------
# ~git/diplomacy/RunDiplomacy.py
# Kevin McKenzie
# UT Austin
# November 17 2020
# -------------------------------

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

