#!/usr/bin/env python3

# ---------------------------
# ~git/diplomacy/Diplomacy.py
# Kevin McKenzie
# UT Austin
# November 18 2020
# ---------------------------


# ------------
# diplomacy_read
# ------------

def diplomacy_read(full_input):
    """
    reads one line
    s a string
    return a list of strings from each line
    """
    return full_input.split()

# ------------
# diplomacy_eval
# ------------


def diplomacy_eval(all_info):
    """
    takes in a list of lists (each one a line of the input file split into individual strings) and evalues diplomacy problem

    guide to data structures used:

    cities dict:
    key: city name
    value: list of armies moving/staying there

    armies dict:
    key: army name
    0: city starting from
    1: city going to
    2: list of supporting armies

    supporting armies dict:
    key: supporting army name
    0: starting city name
    1: army supported
    2: their own supporters

    result dict:
    key: army name
    value: location

    result: list of lists where first entry of each list is army name and second is result/city they are in
    """
    assert isinstance(all_info, list)
    
    cities, armies, supporting_armies = {},{},{}
    result = []
    
    #two passes to initialiize dicts then fill them with info
    for l in all_info:

        #PRECONDITIONS
        if l == []: #no error for empty lines of input
            continue #pragma: no cover
        assert l[0].isupper() and len(l[0]) == 1
        assert l[1][0].isupper() and len(l[1]) > 1
        assert l[2] in ('Move', 'Hold', 'Support')
        if l[2] == 'Support':
            assert l[3].isupper() and len(l[3]) == 1
        if l[2] == 'Move':
            assert l[3][0].isupper() and len(l[3]) > 1
        if l[2] == 'Hold':
            assert len(l) == 3

        #make empty dicts to be filled in next
        if l[2] == 'Support':
            supporting_armies[l[0]] = ['', '', []]
        else:
            armies[l[0]] = ['', '', []]

        #create empty lists in the cities dict        
        if l[1] not in cities:
            cities[l[1]] = []
        try:
            if l[3] not in cities and len(l[3]) > 1:
                cities[l[3]] = []
        except:
            pass

    #now fill in with specific info for each army and city
    for l in all_info:
        if l == []: #no error for empty lines of input
            continue #pragma: no cover
        if l[2] == 'Support':
            supporting_armies[l[0]][0] = l[1] #where army is
            supporting_armies[l[0]][1] = l[3] #army they are supporting
            cities[l[1]].append(l[0]) #army remains in starting city while supporting army in other city
            if l[3] in armies:
                armies[l[3]][2].append(l[0]) #add army to list of supporting armies for the army being supported
            else:
                supporting_armies[l[3]][2].append(l[0])
                
        if l[2] == 'Hold':
            armies[l[0]][0] = l[1] #starting city
            armies[l[0]][1] = l[1] #moving to same as starting city
            cities[l[1]].append(l[0]) #add to list of armies that are moving to the given city

        if l[2] == 'Move':
            armies[l[0]][0] = l[1] #starting city
            armies[l[0]][1] = l[3] #city being moved to
            cities[l[3]].append(l[0]) #add to list of armies that are moving to the given city

    
    #negate support of armies that are attacked (any armies are moving to the city they are in)
    for s in supporting_armies:
        if cities[supporting_armies[s][0]] != [str(s)]: #check if the supporting army city is being attacked
            for a in armies: #check each army to see if they are being supported
                if s in armies[a][2]:
                    armies[a][2].remove(s)
            for a in supporting_armies: #check each supporting army to see if they are being supported
                if s in supporting_armies[a][2]:
                    supporting_armies[a][2].remove(s)

    #now that all supports are balanced, we can evaluate city by city who has the most support.
    for city in cities: #go city by city
        local_armies = cities[city] #git list of army names
        supports = [] #empty list that will hold number of supports for each army
        for army in local_armies: 
            if army in armies: #get number of supports for each army
                supports.append(len(armies[army][2]))
            else:
                supports.append(len(supporting_armies[army][2]))
        if supports and supports.count(max(supports)) == 1: #if there is no tie, evaluate results killing all but winner
            for i in range(len(local_armies)):
                if supports[i] == max(supports):
                    result.append([local_armies[i],city])
                else:
                    result.append([local_armies[i],'[dead]'])
        else:                                               #if there's a tie, everyone dies!
            for army in local_armies:
                result.append([army,'[dead]'])
                
    #check postcondition
    assert isinstance(result, list)
    return sorted(result) #alphabetize result

# -------------
# diplomacy_print
# -------------


def diplomacy_print(w, result):
    """
    print three strings
    w a writer
    result the list of armies and where they are/if they are dead
    """
    for r in result:
        w.write(r[0] + ' ' + r[1] + '\n')

# -------------
# diplomacy_solve
# -------------


def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    """
    a = []
    for s in r:
        a.append(diplomacy_read(s))
    result = diplomacy_eval(a)
    diplomacy_print(w, result)
