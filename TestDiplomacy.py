#!/usr/bin/env python3

# -------------------------------
# ~git/diplomacy/TestDiplomacy.py
# Kevin McKenzie
# UT Austin
# November 18 2020
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    '''
    Tests all actions and gets 100% coverage.
    diplomacy_solve() calls all other methods in Diplomacy.py so this testing covers every line
    other than two lines used to skip blank lines of input.
    '''
    
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Hold\nC London Support A\nD Austin Support C\nE Boston Move Houston\nF Houston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Madrid\nB [dead]\nC London\nD [dead]\nE Houston\nF [dead]\n')
    
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
